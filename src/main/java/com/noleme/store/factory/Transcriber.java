package com.noleme.store.factory;

/**
 * This class acts as a "translation device" between a store implementation and a model class.
 * The Transcriber class translates from the model into the store implementation data, @see com.noleme.store.factory.Builder for the reverse.
 *
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/06/2018
 * @param <I> An entity class.
 * @param <O> A store-specific representation of data.
 */
public interface Transcriber<I, O>
{
    /**
     * This method is expected to "transcript" an entity into a store-specific representation of data.
     *
     * @param input An entity instance.
     * @return A store-specific representation of the input entity.
     */
    I transcript(O input);
}
