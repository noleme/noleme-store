package com.noleme.store.factory;

/**
 * This class regroups both the Builder and Transcriber API into a single contract.
 * Some components may require it in order to ensure that two-way translation is available.
 *
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 08/05/2018
 * @param <I> A store-specific representation of data.
 * @param <O> An entity class.
 */
public interface Factory <I, O> extends Builder<I, O>, Transcriber<I, O>
{
    /**
     * A helper method for when a factory is not really needed but a contract requires one.
     *
     * @param <I> A store-specific representation of data.
     * @param <O> An entity class.
     * @return An anonymous factory instance which returns null for both build and transcript operations.
     */
    static <I, O> Factory<I, O> nullFactory()
    {
        return new Factory<I, O>()
        {
            @Override public O build(I o) { return null; }
            @Override public I transcript(O i) { return null; }
        };
    }
}
