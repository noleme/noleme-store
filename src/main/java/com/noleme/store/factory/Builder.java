package com.noleme.store.factory;

/**
 * This class acts as a "translation device" between a store implementation and a model class.
 * The Builder class translates from the store implementation data into the model, @see com.noleme.store.factory.Transcriber for the reverse.
 *
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/06/2018
 * @param <I> A store-specific representation of data.
 * @param <O> An entity class.
 */
public interface Builder<I, O>
{
    /**
     * This method is expected to "build" an entity from a store-specific representation of data.
     *
     * @param input A store representation of data coming from the underlying storage.
     * @return An entity matching the provided input data.
     */
    O build(I input);
}
