package com.noleme.store.mongodb;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.noleme.mongodb.MongoDBClient;
import com.noleme.store.Store;
import com.noleme.store.factory.Builder;
import com.noleme.store.factory.Factory;
import com.noleme.store.factory.Transcriber;
import com.noleme.store.query.*;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.*;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 09/05/2018
 * @param <T> An entity class implementing the Identifiable contract.
 */
public abstract class DefaultMongoDBStore<T extends Identifiable<String>> implements MongoDBStore, Store<T>
{
    protected final MongoDBClient client;
    protected final Factory<BasicDBObject, T> factory;
    protected DBCollection collection;
    protected MongoCollection<Document> mongoCollection;

    /**
     *
     * @param client A MongoDBClient instance through which to interact with the MongoDB collection.
     * @param factory A Factory instance responsible to translate entities to and from a MongoDB representation.
     */
    public DefaultMongoDBStore(MongoDBClient client, Factory<BasicDBObject, T> factory)
    {
        this(client, factory, true);
    }

    /**
     *
     * @param client A MongoDBClient instance through which to interact with the MongoDB collection.
     * @param factory A Factory instance responsible to translate entities to and from a MongoDB representation.
     * @param initCollection Whether the collection should be automatically referenced from the provided client upon instantiation, set to false if you want to reference it manually.
     */
    public DefaultMongoDBStore(MongoDBClient client, Factory<BasicDBObject, T> factory, boolean initCollection)
    {
        this.client = client;
        this.factory = factory;
        if (initCollection)
        {
            this.collection = this.client.db().getCollection(this.getCollectionName());
            this.mongoCollection = this.client.mongoDb().getCollection(this.getCollectionName());
        }
    }

    @Override
    public void dropStore()
    {
        this.collection.drop();
    }

    @Override
    public void dropIndexes()
    {
        this.collection.dropIndexes();
    }

    /**
     * Translates a Query description instance into its MongoDB driver equivalent.
     *
     * @param query A query description instance.
     * @return The equivalent query using the MongoDB driver API.
     */
    protected DBObject buildQuery(Query query)
    {
        BasicDBObject mongodbQuery = new BasicDBObject();

        this.buildMongoQuery(mongodbQuery, query.getEqual(), null);

        if (query.getIn() != null)
            this.buildArrayMongoQuery(mongodbQuery, query.getIn(), "$in");
        if (query.getNotIn() != null)
            this.buildArrayMongoQuery(mongodbQuery, query.getNotIn(), "$nin");
        if (query.getAll() != null)
            this.buildArrayMongoQuery(mongodbQuery, query.getAll(), "$all");
        if (query.getNotEqual() != null)
            this.buildMongoQuery(mongodbQuery, query.getNotEqual(), "$ne");
        if (query.getGreaterThan() != null)
            this.buildMongoQuery(mongodbQuery, query.getGreaterThan(), "$gt");
        if (query.getLowerThan() != null)
            this.buildMongoQuery(mongodbQuery, query.getLowerThan(), "$lt");
        if (query.getGreaterThanOrEqual() != null)
            this.buildMongoQuery(mongodbQuery, query.getGreaterThanOrEqual(), "$gte");
        if (query.getLowerThanOrEqual() != null)
            this.buildMongoQuery(mongodbQuery, query.getLowerThanOrEqual(), "$lte");
        if (query.getExists() != null)
            this.buildMongoQuery(mongodbQuery, query.getExists(), "$exists");

        return mongodbQuery;
    }

    /**
     * Enriches the provided MongoDB query with a map of clauses.
     * The nature of the clause in MongoDB dialect is determined by the "wrapper" parameter.
     *
     * @param mongodbQuery A MongoDB query description instance.
     * @param properties A map of property names to clause operands.
     * @param wrapper Nature of the clauses to apply for each listed property and their operand, if set to null it will default to an equality comparison.
     */
    private void buildMongoQuery(BasicDBObject mongodbQuery, Map<String, ?> properties, String wrapper)
    {
        for (Map.Entry<String, ?> e : properties.entrySet())
        {
            String key = e.getKey();
            Object value = this.processValue(e.getValue());

            /*
             * If there is a pre-existing clause within a wrapper, we just add our new clause to it.
             * Otherwise we consider the previous clause to be an "equals" clause, so we abort adding lower-priority conditions.
             * If the new clause is an "equals" clause, we need don't need this (we'll just replace the pre-existing clause in the "else").
             */
            if (mongodbQuery.containsField(key) && mongodbQuery.get(key) instanceof BasicDBObject && wrapper != null)
            {
                BasicDBObject clause = (BasicDBObject) mongodbQuery.get(key);
                clause.put(wrapper, value);
            }
            else {
                Object mongoValue = wrapper == null ? value : new BasicDBObject(wrapper, value);
                mongodbQuery.put(key, mongoValue);
            }
        }
    }

    /**
     * Enriches the provided MongoDB query with a map of clauses for which the operands need to be wrapped in a BasicDBList.
     * The nature of the clause in MongoDB dialect is determined by the "wrapper" parameter.
     *
     * @param mongodbQuery A MongoDB query description instance.
     * @param properties A map of property names to clause operands.
     * @param wrapper Nature of the clauses to apply for each listed property and their operand, if set to null it will default to an equality comparison.
     */
    private void buildArrayMongoQuery(BasicDBObject mongodbQuery, Map<String, Collection<?>> properties, String wrapper)
    {
        for (Map.Entry<String, Collection<?>> e : properties.entrySet())
        {
            String key = e.getKey();
            BasicDBList queryValues = new BasicDBList();

            for (Object v : e.getValue())
            {
                Object value = this.processValue(v);
                queryValues.add(value);
            }

            Object mongoValue = wrapper == null ? queryValues : new BasicDBObject(wrapper, queryValues);
            mongodbQuery.put(key, mongoValue);
        }
    }

    /**
     * Ensures a proper representation of values before their insertion in a MongoDB query description.
     * Mostly handles the encapsulation of Identifier instances into a MongoDB ObjectId.
     *
     * @param value The value to be considered.
     * @return The same value, potentially altered in order to fit in a MongoDB query description.
     */
    @SuppressWarnings("unchecked")
    private Object processValue(Object value)
    {
        if (value instanceof Identifier && ((Identifier) value).hasType(String.class))
            value = this.transcriptIdentifier(((Identifier<String>) value).value());

        return value;
    }

    /**
     * Translates a Filter's property restriction list into a MongoDB projection equivalent.
     *
     * @param filter A filter description instance
     * @return The equivalent projection using the MongoDB driver API.
     */
    protected BasicDBObject buildProjection(Filter filter)
    {
        if (filter == null)
            return null;

        BasicDBObject projection = new BasicDBObject()
            .append(this.getIdentifiableKey(), 1)
        ;

        if (filter.includeAll())
            projection = null;
        else {
            for (String property : filter.getProperties())
                projection.append(property, 1);
            if (!filter.getSlices().isEmpty()) {
                for (Map.Entry<String, List<Integer>> entry : filter.getSlices().entrySet()) {
                    projection.append(
                            entry.getKey(),
                            new BasicDBObject("$slice", entry.getValue().toArray(new Integer[0]))
                    );

                }
            }

        }

        return projection;
    }

    protected BasicDBObject buildHint(Filter filter)
    {
        if (filter == null)
            return null;
        BasicDBObject hint = new BasicDBObject();
        hint.putAll(filter.getHint());
        return hint;
    }

    /**
     * Finds a single entity using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @return The entity if found, or null otherwise.
     */
    protected T findByQuery(Query query, Filter filter)
    {
        return this.findByDbObject(this.buildQuery(query), filter);
    }

    /**
     * Finds a single entity using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance to use for creating entities from the query results.
     * @return The entity if found, or null otherwise.
     */
    protected <O> O findByQuery(Query query, Filter filter, Builder<BasicDBObject, O> builder)
    {
        return this.findByDbObject(this.buildQuery(query), filter, builder);
    }

    /**
     * Finds a single entity using a MongoDB query description and a query modifier.
     *
     * @param query The MongoDB query description.
     * @param filter A Filter query modifier.
     * @return The entity if found, or null otherwise.
     */
    protected T findByDbObject(DBObject query, Filter filter)
    {
        T entity = this.findByDbObject(query, filter, this.factory);

        entity = this.afterFind(entity);

        return entity;
    }

    /**
     * Finds a single entity using a MongoDB query description and a query modifier.
     *
     * @param query The MongoDB query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance to use for creating entities from the query results.
     * @return The entity if found, or null otherwise.
     */
    protected <O> O findByDbObject(DBObject query, Filter filter, Builder<BasicDBObject, O> builder)
    {
        DBObject found = this.collection.findOne(query, this.buildProjection(filter));

        if (found == null)
            return null;

        return builder.build((BasicDBObject) found);
    }

    /**
     * Checks whether an entity exists using a custom query description.
     *
     * @param query The query description.
     * @return True if the entity exists, false otherwise.
     */
    protected boolean hasByQuery(Query query)
    {
        return this.hasByDbObject(this.buildQuery(query));
    }

    /**
     * Checks whether an entity exists using a MongoDB query description.
     *
     * @param query The MongoDB query description.
     * @return True if the entity exists, false otherwise.
     */
    protected boolean hasByDbObject(DBObject query)
    {
        return this.collection.find(query).limit(1).hasNext();
    }

    /**
     * Finds a collection of entities using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @return A collection containing all entities matching the provided query.
     */
    protected List<T> listByQuery(Query query, Filter filter)
    {
        return this.listByDbObject(this.buildQuery(query), filter);
    }

    /**
     * Finds a collection of entities using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance to use for creating entities from the query results.
     * @return A collection containing all entities matching the provided query.
     */
    protected <O> List<O> listByQuery(Query query, Filter filter, Builder<BasicDBObject, O> builder)
    {
        return this.listByDbObject(this.buildQuery(query), filter, builder);
    }

    /**
     * Finds a collection of entities using a MongoDB query description and a query modifier.
     *
     * @param query The MongoDB query description.
     * @param filter A Filter query modifier.
     * @return A collection containing all entities matching the provided query.
     */
    protected List<T> listByDbObject(DBObject query, Filter filter)
    {
        List<T> list = this.listByDbObject(query, filter, this.factory);

        list = this.afterList(list);

        return list;
    }

    /**
     * Finds a collection of entities using a MongoDB query description and a query modifier.
     *
     * @param query The MongoDB query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance to use for creating entities from the query results.
     * @param <O> An arbitrary return type.
     * @return A collection containing all entities matching the provided query, and built using the provided builder.
     */
    protected <O> List<O> listByDbObject(DBObject query, Filter filter, Builder<BasicDBObject, O> builder)
    {
        List<O> items = new ArrayList<>();

        DBCursor cursor = this.cursorByDbObject(query, filter);

        while (cursor.hasNext())
            items.add(builder.build((BasicDBObject) cursor.next()));

        cursor.close();

        return items;
    }

    /**
     * Maps a collection of entities to their IDs, using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @return A map of {id : entities} containing entities matching the provided query.
     */
    protected Map<String, T> mapByQuery(Query query, Filter filter)
    {
        return this.mapByDbObject(this.buildQuery(query), filter);
    }

    /**
     * Maps a collection of entities to their IDs, using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance to use for creating entities from the query results.
     * @return A map of {id : entities} containing entities matching the provided query.
     */
    protected Map<String, T> mapByQuery(Query query, Filter filter, Builder<BasicDBObject, T> builder)
    {
        return this.mapByDbObject(this.buildQuery(query), filter, builder);
    }

    /**
     * Maps a collection of entities to an arbitrary key, using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance to use for creating entities from the query results.
     * @param marker An IdentityMarker instance to use for mapping the resulting entities.
     * @param <K> An arbitrary key type.
     * @param <V> An arbitrary return type.
     * @return A map of {identifier : entities} containing entities matching the provided query, built using the provided builder, mapped using the provided identity marker.
     */
    protected <K, V> Map<K, V> mapByQuery(Query query, Filter filter, Builder<BasicDBObject, V> builder, IdentityMarker<BasicDBObject, K> marker)
    {
        return this.mapByDbObject(this.buildQuery(query), filter, builder, marker);
    }

    /**
     * Maps a collection of entities to their IDs, using a MongoDB query description and a query modifier.
     *
     * @param query The MongoDB query description.
     * @param filter A Filter query modifier.
     * @return A map of {id : entities} containing entities matching the provided query.
     */
    protected Map<String, T> mapByDbObject(DBObject query, Filter filter)
    {
        return this.mapByDbObject(query, filter, this.factory);
    }

    /**
     * Maps a collection of entities to their IDs, using a MongoDB query description and a query modifier.
     * Note: this method is parameterized with the restricted T parameter (and not an open type like findByDbObject or listByDbObject) because it relies on the assumption that the output type is an Identifiable that can be ordered into a Map which keys are the entities' unique id.
     *
     * @param query The MongoDB query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance to use for creating entities from the query results.
     * @return A map of {id : entities} containing entities matching the provided query.
     */
    protected Map<String, T> mapByDbObject(DBObject query, Filter filter, Builder<BasicDBObject, T> builder)
    {
        Map<String, T> items = new HashMap<>();

        DBCursor cursor = this.cursorByDbObject(query, filter);

        while (cursor.hasNext())
        {
            T item = builder.build((BasicDBObject) cursor.next());
            items.put(item.getId(), item);
        }

        cursor.close();

        items = this.afterMap(items);

        return items;
    }

    /**
     * Maps a collection of entities to an arbitrary key, using a MongoDB query description and a query modifier.
     *
     * @param query The MongoDB query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance to use for creating entities from the query results.
     * @param marker An IdentityMarker instance to use for mapping the resulting entities to an arbitrary key.
     * @param <K> An arbitrary key type.
     * @param <V> An arbitrary return type.
     * @return A map of {id : entities} containing entities matching the provided query.
     */
    protected <K, V> Map<K, V> mapByDbObject(DBObject query, Filter filter, Builder<BasicDBObject, V> builder, IdentityMarker<BasicDBObject, K> marker)
    {
        Map<K, V> items = new HashMap<>();

        DBCursor cursor = this.cursorByDbObject(query, filter);

        while (cursor.hasNext())
        {
            BasicDBObject obj = (BasicDBObject) cursor.next();
            V item = builder.build(obj);
            items.put(marker.identify(obj), item);
        }

        cursor.close();

        return items;
    }

    /**
     * Produces a MongoDB DBCursor for the provided custom query description and query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @return A MongoDB DBCursor.
     */
    protected DBCursor cursorByQuery(Query query, Filter filter)
    {
        return this.cursorByDbObject(this.buildQuery(query), filter);
    }

    /**
     * Produces a MongoDB DBCursor for the provided MongoDB query description and query modifier.
     *
     * @param query The MongoDB query description.
     * @param filter A Filter query modifier.
     * @return A MongoDB DBCursor.
     */
    protected DBCursor cursorByDbObject(DBObject query, Filter filter)
    {
        DBCursor cursor = this.collection.find(query, this.buildProjection(filter));

        if (filter != null)
        {
            if (filter.sort() != null)
                cursor.sort(new BasicDBObject(filter.sort(), filter.order().getCode()));

            if (filter.max() >= 0)
            {
                if (filter.sort() == null)
                    cursor.sort(new BasicDBObject(this.getIdentifiableKey(), filter.order().getCode()));
                cursor.limit(filter.max());
            }
            if (filter.skip() >= 0)
                cursor.skip(filter.skip());
            if (filter.getHint() != null)
                cursor.hint(this.buildHint(filter));
        }

        return cursor;
    }

    @Override
    public T find(String id)
    {
        return this.find(id, null);
    }

    @Override
    public T find(String id, Filter filter)
    {
        return this.find(new Query(this.getIdentifiableKey(), this.transcriptIdentifier(id)), filter);
    }

    @Override
    public T find(Query query)
    {
        return this.find(query, null);
    }

    @Override
    public T find(Query query, Filter filter)
    {
        return this.findByQuery(query, filter);
    }

    /**
     * Finds a single entity using a custom query description, a query modifier and a custom entity Builder.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance responsible for creating entities from the query results.
     * @param <O> An entity result type.
     * @return The entity if found, or null otherwise.
     */
    public <O> O find(Query query, Filter filter, Builder<BasicDBObject, O> builder)
    {
        return this.findByQuery(query, filter, builder);
    }

    @Override
    public boolean has(String id)
    {
        return this.has(new Query(this.getIdentifiableKey(), this.transcriptIdentifier(id)));
    }

    @Override
    public boolean has(Query query)
    {
        return this.hasByQuery(query);
    }

    @Override
    public List<T> list(Collection<String> ids)
    {
        return this.list(ids, null);
    }

    @Override
    public List<T> list(Collection<String> ids, Filter filter)
    {
        BasicDBList queryIds = new BasicDBList();
        for (String id : ids)
            queryIds.add(this.transcriptIdentifier(id));

        return this.listByQuery(new Query().in(this.getIdentifiableKey(), queryIds), filter);
    }

    @Override
    public List<T> list(Query query)
    {
        return this.list(query, null);
    }

    @Override
    public List<T> list(Query query, Filter filter)
    {
        return this.listByQuery(query, filter);
    }

    /**
     * Finds a collection of entities using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance responsible for creating entities from the query results.
     * @param <O> An arbitrary result type.
     * @return A collection containing all entities matching the provided query.
     */
    public <O> List<O> list(Query query, Filter filter, Builder<BasicDBObject, O> builder)
    {
        return this.listByQuery(query, filter, builder);
    }

    @Override
    public Map<String, T> map(Collection<String> ids)
    {
        return this.map(ids, null);
    }

    @Override
    public Map<String, T> map(Collection<String> ids, Filter filter)
    {
        BasicDBList queryIds = new BasicDBList();
        for (String id : ids)
            queryIds.add(this.transcriptIdentifier(id));

        return this.mapByQuery(new Query().in(this.getIdentifiableKey(), queryIds), filter);
    }

    @Override
    public Map<String, T> map(Query query)
    {
        return this.map(query, null);
    }

    @Override
    public Map<String, T> map(Query query, Filter filter)
    {
        return this.mapByQuery(query, filter);
    }

    /**
     * Maps a collection of entities to their IDs, using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance responsible for creating entities from the query results.
     * @return A map of {id : entities} containing entities matching the provided query.
     */
    public Map<String, T> map(Query query, Filter filter, Builder<BasicDBObject, T> builder)
    {
        return this.mapByQuery(query, filter, builder);
    }

    /**
     * Maps a collection of entities to their IDs, using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @param builder A Builder instance responsible for creating entities from the query results.
     * @param marker An IdentityMarker instance to use for mapping the resulting entities.
     * @param <O> An arbitrary result type.
     * @return A map of {id : entities} containing entities matching the provided query.
     */
    public <O> Map<String, O> map(Query query, Filter filter, Builder<BasicDBObject, O> builder, IdentityMarker<BasicDBObject, String> marker)
    {
        return this.mapByQuery(query, filter, builder, marker);
    }

    @Override
    public void put(T item)
    {
        this.put(item, this.factory);
    }

    /**
     * Persists an entity.
     * If the entity has an ID it will update the corresponding record with the provided data, otherwise it will be created.
     *
     * @param item The entity to be persisted.
     * @param transcriber A Transcriber instance responsible for producing a MongoDB representation of the provided entity.
     */
    public void put(T item, Transcriber<BasicDBObject, T> transcriber)
    {
        item = this.beforePut(item);

        if (item.getId() != null)
        {
            BasicDBObject query = new BasicDBObject(this.getIdentifiableKey(), this.transcriptIdentifier(item.getId()));
            BasicDBObject update = new BasicDBObject("$set", transcriber.transcript(item));
            this.collection.update(query, update, true, false);
        }
        else {
            BasicDBObject save = transcriber.transcript(item);
            this.collection.save(save);
        }
    }

    @Override
    public void put(Collection<T> items)
    {
        this.put(items, this.factory);
    }

    /**
     * Persists a collection of entities.
     * If the entities have an ID it will update the corresponding record with the provided data, otherwise it will be created.
     *
     * @param items The entities to be persisted.
     * @param transcriber A Transcriber instance responsible for producing a MongoDB representation of the provided entity.
     */
    public void put(Collection<T> items, Transcriber<BasicDBObject, T> transcriber)
    {
        BulkWriteOperation op = this.collection.initializeUnorderedBulkOperation();

        for (T item : items)
        {
            item = this.beforePut(item);

            if (item.getId() != null)
            {
                BasicDBObject query = new BasicDBObject(this.getIdentifiableKey(), this.transcriptIdentifier(item.getId()));
                BasicDBObject update = new BasicDBObject("$set", transcriber.transcript(item));

                op.find(query).upsert().updateOne(update);
            }
            else
                op.insert(transcriber.transcript(item));
        }
        op.execute();
    }

    @Override
    public void remove(String id)
    {
        this.collection.remove(new BasicDBObject(this.getIdentifiableKey(), this.transcriptIdentifier(id)));
    }

    @Override
    public void remove(Query query)
    {
        this.collection.remove(this.buildQuery(query));
    }

    @Override
    public long count()
    {
        return this.collection.count();
    }

    @Override
    public long count(Query query)
    {
        return this.collection.count(this.buildQuery(query));
    }

    /**
     * Returns the name of the MongoDB collection.
     *
     * @return The name of the MongoDB collection.
     */
    protected abstract String getCollectionName();

    /**
     * Returns the document field name used for the entity's Identifiable property.
     *
     * @return The document field name used for the entity's Identifiable property.
     */
    protected String getIdentifiableKey()
    {
        return "_id";
    }

    /**
     *
     * @param identifier
     * @return
     */
    protected Object transcriptIdentifier(String identifier)
    {
        return new ObjectId(identifier);
    }

    /**
     * Hook for altering an entity after it is retrieved.
     *
     * @param item The item that was retrieved.
     * @return The item after any eventual modification.
     */
    protected T afterFind(T item)
    {
        this.afterRetrieval(Collections.singletonList(item));
        return item;
    }

    /**
     * Hook for altering an entity list after it is retrieved.
     *
     * @param list The entity list that was retrieved.
     * @return The list after any eventual modification.
     */
    protected List<T> afterList(List<T> list)
    {
        this.afterRetrieval(list);
        return list;
    }

    /**
     * Hook for altering an entity map after it is retrieved.
     *
     * @param map The entity map that was retrieved.
     * @return The map after any eventual modification.
     */
    protected Map<String, T> afterMap(Map<String, T> map)
    {
        this.afterRetrieval(map.values());
        return map;
    }

    /**
     * Method for extending both find, list and map with a single implementations of eager loading logic.
     *
     * @param collection The entity collection that was retrieved.
     * @param <C>
     */
    protected <C extends Collection<T>> void afterRetrieval(C collection)
    {
        /* By default it doesn't do anything. */
    }

    @Deprecated
    protected <C extends Collection<T>> void afterCollection(C collection)
    {
        this.afterRetrieval(collection);
    }

    /**
     * Hook for altering an entity before it is persisted.
     *
     * @param item The item to be persisted.
     * @return The item after any eventual modification.
     */
    protected T beforePut(T item)
    {
        return item;
    }
}
