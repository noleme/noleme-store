package com.noleme.store.mongodb;

/**
 * This contract is expected to be implemented by all MongoDB based stores.
 *
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 29/04/2017
 */
public interface MongoDBStore
{
    /**
     * Creates all indexes for this store.
     */
    void createIndexes();

    /**
     * Drops all indexes for this store.
     */
    void dropIndexes();

    /**
     * Drops the store.
     */
    void dropStore();
}
