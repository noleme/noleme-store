package com.noleme.store.mongodb;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.noleme.store.factory.Builder;
import com.noleme.store.factory.Transcriber;
import org.bson.types.ObjectId;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class provides a set of helper methods for common building and transcription operations.
 * Operations involving entity manipulation may require an appropriate Builder or Transcriber instance.
 *
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 09/05/2018
 */
public final class MongoDBFactories
{
    private MongoDBFactories() {}

    /* Build methods */

    /**
     * Fills a collection with entities or values from a MongoDB BasicDBList, using an appropriate Builder instance.
     *
     * @param collection A container instance.
     * @param builder A builder responsible for producing entities or values from a given type.
     * @param list A MongoDB list containing representations of entities or values.
     * @param <T> The source type
     * @param <U> The entity type.
     * @param <C> The collection container type.
     * @return The provided collection containing all entities found in the provided BasicDBList.
     */
    @SuppressWarnings("unchecked")
    public static <T, U, C extends Collection<U>> C buildCollection(C collection, Builder<T, U> builder, BasicDBList list)
    {
        if (list == null)
            return collection;
        for (Object o : list)
            collection.add(builder.build((T) o));
        return collection;
    }

    /**
     * Fills a collection with values from a MongoDB BasicDBList.
     *
     * @param collection A container instance.
     * @param list A MongoDB list.
     * @param <T> The value type.
     * @param <C> The collection container type.
     * @return The provided collection containing all values found in the provided BasicDBList.
     */
    @SuppressWarnings("unchecked")
    public static <T, C extends Collection<T>> C buildCollection(C collection, BasicDBList list)
    {
        if (list == null)
            return collection;
        for (Object o : list)
            collection.add((T) o);
        return collection;
    }

    /**
     * Fills a collection with String IDs from a MongoDB BasicDBList containing ObjectId instances.
     *
     * @param collection A container instance.
     * @param list A MongoDB list containing ObjectId instances.
     * @param <C> The collection container type.
     * @return The provided collection containing all IDs found in the provided BasicDBList.
     */
    public static <C extends Collection<String>> C buildIdCollection(C collection, BasicDBList list)
    {
        return buildCollection(collection, ObjectId::toHexString, list);
    }

    /**
     * Fills a collection with enum values from a MongoDB BasicDBList.
     *
     * @param collection A container instance.
     * @param type An enum type.
     * @param list A MongoDB list containing string representations of enum entries.
     * @param <E> The enum type.
     * @param <C> The collection container type.
     * @return The provided collection containing all enum values found in the provided BasicDBList.
     */
    public static <E extends Enum<E>, C extends Collection<E>> C buildEnumCollection(C collection, Class<E> type, BasicDBList list)
    {
        return buildCollection(collection, (Builder<String, E>) input -> Enum.valueOf(type, input), list);
    }

    /**
     * Fills a collection with enum values from a MongoDB BasicDBList.
     *
     * @param collection A container instance.
     * @param type An enum type.
     * @param list A MongoDB list containing string representations of enum entries.
     * @param <E> The enum type.
     * @param <C> The collection container type.
     * @return The provided collection containing all enum values found in the provided BasicDBList.
     * @deprecated Use MongoDBFactories.buildEnumCollection instead
     */
    @Deprecated
    public static <E extends Enum<E>, C extends Collection<E>> C buildCollection(C collection, Class<E> type, BasicDBList list)
    {
        return buildEnumCollection(collection, type, list);
    }

    /**
     * Fills a collection with values from a MongoDB BasicDBList, using an appropriate Builder instance.
     *
     * @param collection A container instance.
     * @param builder A builder responsible for producing entities from a BasicDBObject.
     * @param list A MongoDB list containing representations of entities.
     * @param <T> The value type.
     * @param <C> The collection container type.
     * @return The provided collection containing all values found in the provided BasicDBList.
     * @deprecated Use MongoDBFactories.buildCollection instead
     */
    @Deprecated
    public static <T, C extends Collection<T>> C buildValueCollection(C collection, Builder<Object, T> builder, BasicDBList list)
    {
        return buildCollection(collection, builder, list);
    }

    /**
     * Fills a map with entities or values from a MongoDB BasicDBObject, using an appropriate Builder instance.
     *
     * @param map A container instance.
     * @param builder A builder responsible for producing entities from a BasicDBObject.
     * @param object A MongoDB object containing representations of entities.
     * @param <T> The source type
     * @param <U> The entity type.
     * @param <M> The map container type.
     * @return The provided map containing all entities or values found in the provided BasicDBObject, retaining the same keys.
     */
    @SuppressWarnings("unchecked")
    public static <T, U, M extends Map<String, U>> M buildMap(M map, Builder<T, U> builder, BasicDBObject object)
    {
        if (object == null)
            return map;
        for (String key : object.keySet())
        {
            T obj = (T) object.get(key);
            map.put(key, builder.build(obj));
        }
        return map;
    }

    /**
     * Fills a map of values from a MongoDB BasicDBObject.
     *
     * @param map A container instance.
     * @param object A MongoDB object.
     * @param <T> The value type.
     * @param <M> The map container type.
     * @return The provided map containing all values found in the provided BasicDBObject, retaining the same keys.
     */
    @SuppressWarnings("unchecked")
    public static <T, M extends Map<String, T>> M buildMap(M map, BasicDBObject object)
    {
        if (object == null)
            return map;
        for (String key : object.keySet())
            map.put(key, (T) object.get(key));
        return map;
    }

    /**
     * Fills a map with String IDs from a MongoDB BasicDBObject containing ObjectId instances.
     *
     * @param map A container instance.
     * @param object A MongoDB object containing ObjectId instances.
     * @param <M> The map container type.
     * @return The provided map containing all IDs found in the provided BasicDBObject, retaining the same keys.
     */
    public static <M extends Map<String, String>> M buildIdMap(M map, BasicDBObject object)
    {
        return buildMap(map, ObjectId::toHexString, object);
    }

    /**
     * Fills a map with enum values from a MongoDB BasicDBObject.
     *
     * @param map A container instance.
     * @param type An enum type.
     * @param object A MongoDB object.
     * @param <E> The enum type.
     * @param <M> The map container type.
     * @return The provided map containing all enum values found in the provided BasicDBObject, retaining the same keys.
     */
    public static <E extends Enum<E>, M extends Map<String, E>> M buildEnumMap(M map, Class<E> type, BasicDBObject object)
    {
        return buildMap(map, (Builder<String, E>) input -> Enum.valueOf(type, input), object);
    }

    /**
     * Fills a map with enum values from a MongoDB BasicDBObject.
     *
     * @param map A container instance.
     * @param type An enum type.
     * @param object A MongoDB object.
     * @param <E> The enum type.
     * @param <M> The map container type.
     * @return The provided map containing all enum values found in the provided BasicDBObject, retaining the same keys.
     * @deprecated Use MongoDBFactories.buildEnumMap instead
     */
    @Deprecated
    public static <E extends Enum<E>, M extends Map<String, E>> M buildMap(M map, Class<E> type, BasicDBObject object)
    {
        for (String key : object.keySet())
            map.put(key, Enum.valueOf(type, object.getString(key)));
        return map;
    }

    /* Transcription methods */

    /**
     * Produces a BasicDBList containing representations of entities from a Collection container, using an appropriate Transcriber instance.
     *
     * @param collection A container instance.
     * @param transcriber A transcriber responsible for producing BasicDBObjects from an entity.
     * @param <T> The entity type.
     * @param <C> The collection container type.
     * @return A BasicDBList containing representations of entities from the provided container.
     */
    public static <T, U, C extends Collection<U>> BasicDBList transcriptCollection(C collection, Transcriber<T, U> transcriber)
    {
        BasicDBList list = new BasicDBList();
        for (U object : collection)
            list.add(transcriber.transcript(object));
        return list;
    }

    /**
     * Produces a BasicDBList containing values from a Collection container.
     *
     * @param collection A container instance.
     * @return The BasicDBList containing values from the provided container.
     */
    public static BasicDBList transcriptCollection(Collection collection)
    {
        BasicDBList list = new BasicDBList();
        list.addAll(collection);
        return list;
    }

    /**
     * Produces a BasicDBList containing ObjectId instances derived from a Collection of String IDs.
     *
     * @param collection A container instance.
     * @return A BasicDBList containing ObjectId instances generated from the provided container.
     */
    public static BasicDBList transcriptIdCollection(Collection<String> collection)
    {
        return transcriptCollection(collection, ObjectId::new);
    }

    /**
     * Produces a BasicDBList containing String instances derived from a Collection of Enums.
     *
     * @param collection  A container instance.
     * @param <E> The Enum type
     * @param <C> The collection container type
     * @return A BasicDBList containing representations of Enum instances as strings.
     */
    public static <E extends Enum<E>, C extends Collection<E>> BasicDBList transcriptEnumCollection(C collection)
    {
        return transcriptCollection(collection, Enum::name);
    }

    /**
     *
     * @param collection An unordered container instance.
     * @param transcriber A transcriber responsible for producing BasicDBObjects from an entity.
     * @param comparator A comparator responsible for comparing instances from within the container when performing the sort
     * @param <T> The entity type.
     * @param <C> The collection container type.
     * @return A BasicDBList containing representations of entities from the provided container.
     */
    public static <T, U, C extends Collection<U>> BasicDBList transcriptUnorderedCollection(C collection, Transcriber<T, U> transcriber, Comparator<U> comparator)
    {
        List<U> sortedCollection = collection.stream()
            .sorted(comparator)
            .collect(Collectors.toList())
        ;

        BasicDBList list = new BasicDBList();
        for (U object : sortedCollection)
            list.add(transcriber.transcript(object));
        return list;
    }

    /**
     *
     * @param collection An unordered container instance.
     * @param transcriber A transcriber responsible for producing BasicDBObjects from an entity.
     * @param <T> The entity type.
     * @param <C> The collection container type.
     * @return A BasicDBList containing representations of entities from the provided container.
     */
    public static <T, U, C extends Collection<U>> BasicDBList transcriptUnorderedCollection(C collection, Transcriber<T, U> transcriber)
    {
        return transcriptUnorderedCollection(collection, transcriber, Comparator.comparingInt(Object::hashCode));
    }

    /**
     * Produces a BasicDBObject containing representations of entities or values from a Map container, using an appropriate Transcriber instance.
     *
     * @param map A container instance.
     * @param transcriber A transcriber responsible for producing BasicDBObjects from an entity.
     * @param <U> The entity or value type.
     * @param <M> The map container type.
     * @return The BasicDBObject containing representations of entities from the provided container.
     */
    public static <T, U, M extends Map<String, U>> BasicDBObject transcriptMap(M map, Transcriber<T, U> transcriber)
    {
        BasicDBObject object = new BasicDBObject();
        for (Map.Entry<String, U> e : map.entrySet())
            object.put(e.getKey(), transcriber.transcript(e.getValue()));
        return object;
    }

    /**
     * Produces a BasicDBObject containing values from a Map container, retaining the same keys.
     *
     * @param map A container instance.
     * @return A BasicDBObject containing values from the provided container.
     */
    public static BasicDBObject transcriptMap(Map<String, ?> map)
    {
        BasicDBObject object = new BasicDBObject();
        for (Map.Entry<String, ?> e : map.entrySet())
            object.put(e.getKey(), e.getValue());
        return object;
    }

    /**
     * Produces a BasicDBObject containing ObjectId instances derived from a Map of String IDs, retaining the same keys.
     *
     * @param map A container instance.
     * @return A BasicDBObject containing ObjectId instances generated from the provided container.
     */
    public static BasicDBObject transcriptIdMap(Map<String, String> map)
    {
        return transcriptMap(map, ObjectId::new);
    }

    /**
     * Produces a BasicDBObject containing ObjectId instances derived from a Map of String IDs, retaining the same keys.
     *
     * @param map A container instance.
     * @return A BasicDBObject containing ObjectId instances generated from the provided container.
     */
    public static <E extends Enum<E>> BasicDBObject transcriptEnumMap(Map<String, E> map)
    {
        return transcriptMap(map, Enum::name);
    }
}
