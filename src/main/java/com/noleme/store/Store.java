package com.noleme.store;

import com.noleme.store.query.Filter;
import com.noleme.store.query.Query;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 09/05/2018
 * @param <T> The store's entity type.
 */
public interface Store <T>
{
    /**
     * Finds a single entity using its ID.
     *
     * @param id The entity's ID.
     * @return The entity if found, or null otherwise.
     */
    T find(String id);

    /**
     * Finds a single entity using its ID and a query modifier.
     *
     * @param id The entity's ID.
     * @param filter A Filter query modifier.
     * @return The entity if found, or null otherwise.
     */
    T find(String id, Filter filter);

    /**
     * Finds a single entity using a custom query description.
     *
     * @param query The query description.
     * @return The entity if found, or null otherwise.
     */
    T find(Query query);

    /**
     * Finds a single entity using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @return The entity if found, or null otherwise.
     */
    T find(Query query, Filter filter);

    /**
     * Checks whether an entity exists using its ID.
     *
     * @param id The entity's ID.
     * @return True if the entity exists, false otherwise.
     */
    boolean has(String id);

    /**
     * Checks whether an entity exists using a custom query description.
     *
     * @param query The query description.
     * @return True if the entity exists, false otherwise.
     */
    boolean has(Query query);

    /**
     * Finds a collection of entities using their IDs.
     *
     * @param ids The entities IDs.
     * @return A collection containing all entities matching the provided IDs.
     */
    List<T> list(Collection<String> ids);

    /**
     * Finds a collection of entities using their IDs and a query modifier.
     *
     * @param ids The entities IDs.
     * @param filter A Filter query modifier.
     * @return A collection containing all entities matching the provided IDs.
     */
    List<T> list(Collection<String> ids, Filter filter);

    /**
     * Finds a collection of entities using a custom query description.
     *
     * @param query The query description.
     * @return A collection containing all entities matching the provided query.
     */
    List<T> list(Query query);

    /**
     * Finds a collection of entities using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @return A collection containing all entities matching the provided query.
     */
    List<T> list(Query query, Filter filter);

    /**
     * Maps a collection of entities to their IDs.
     *
     * @param ids The entities IDs.
     * @return A map of {id : entities} containing entities matching the provided IDs.
     */
    Map<String, T> map(Collection<String> ids);

    /**
     * Maps a collection of entities to their IDs, using their IDs and a query modifier.
     *
     * @param ids The entities IDs.
     * @param filter A Filter query modifier.
     * @return A map of {id : entities} containing entities matching the provided IDs.
     */
    Map<String, T> map(Collection<String> ids, Filter filter);

    /**
     * Maps a collection of entities to their IDs, using a custom query description.
     *
     * @param query The query description.
     * @return A map of {id : entities} containing entities matching the provided query.
     */
    Map<String, T> map(Query query);

    /**
     * Maps a collection of entities to their IDs, using a custom query description and a query modifier.
     *
     * @param query The query description.
     * @param filter A Filter query modifier.
     * @return A map of {id : entities} containing entities matching the provided query.
     */
    Map<String, T> map(Query query, Filter filter);

    /**
     * Persists an entity.
     * If the entity has an ID it will update the corresponding record with the provided data, otherwise it will be created.
     *
     * @param item The entity to be persisted.
     */
    void put(T item);

    /**
     * Persists a collection of entities.
     * If the entities have an ID it will update the corresponding record with the provided data, otherwise it will be created.
     *
     * @param items The entities to be persisted.
     */
    void put(Collection<T> items);

    /**
     * Removes an entity using its ID.
     *
     * @param id The entity's ID.
     */
    void remove(String id);

    /**
     * Removes entities using a custom query description.
     *
     * @param query The query description.
     */
    void remove(Query query);

    /**
     * Counts all entities.
     *
     * @return The number of entities in the store.
     */
    long count();

    /**
     * Counts all entities matching the custom query description.
     *
     * @param query The query description.
     * @return The number of entities matching the provided query in the store.
     */
    long count(Query query);
}
