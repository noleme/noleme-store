package com.noleme.store.query;

/**
 * A contract used by some stores as a mean to access an entity's unique identifier.
 *
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 09/05/2018
 */
public interface Identifiable <T>
{
    /**
     * Returns a unique ID that can be relied upon for querying purposes.
     *
     * @return The entity's unique ID.
     */
    T getId();
}
