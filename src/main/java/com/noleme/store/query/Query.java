package com.noleme.store.query;

import java.util.*;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 09/05/2018
 */
public class Query
{
    private final Map<String, Object> equal;
    private Map<String, Collection<?>> in;
    private Map<String, Collection<?>> notIn;
    private Map<String, Collection<?>> all;
    private Map<String, Object> notEqual;
    private Map<String, Object> greaterThan;
    private Map<String, Object> lowerThan;
    private Map<String, Object> greaterThanOrEqual;
    private Map<String, Object> lowerThanOrEqual;
    private Map<String, Boolean> exists;

    /**
     * 
     */
    public Query()
    {
        this.equal = new HashMap<>();
    }

    /**
     * Instantiation with an "equal" clause.
     * 
     * @param property The property name.
     * @param value The value to which the property is expected to be equal.
     */
    public Query(String property, Object value)
    {
        this();
        this.equal(property, value);
    }

    /**
     * Adds an "equal" clause to the query.
     * 
     * @param property The property name. 
     * @param value The value to which the property is expected to be equal.
     * @return The Query instance. 
     */
    public Query equal(String property, Object value)
    {
        this.equal.put(property, value);
        return this;
    }

    /**
     * Adds a "not equal" clause to the query.
     *
     * @param property The property name.
     * @param value The value to which the property is expected not to be equal.
     * @return The Query instance.
     */
    public Query notEqual(String property, Object value)
    {
        if (this.notEqual == null)
            this.notEqual = new HashMap<>();
        this.notEqual.put(property, value);
        return this;
    }

    /**
     * Adds an "in" clause to the query.
     *
     * @param property The property name.
     * @param values The values to which the property is expected to be equal.
     * @return The Query instance.
     */
    public Query in(String property, Object... values)
    {
        return this.in(property, Arrays.asList(values));
    }

    /**
     * Adds an "in" clause to the query.
     *
     * @param property The property name.
     * @param values The values to which the property is expected to be equal.
     * @return The Query instance.
     */
    public Query in(String property, Collection<?> values)
    {
        if (this.in == null)
            this.in = new HashMap<>();
        this.in.put(property, values);
        return this;
    }

    /**
     * Adds an "all" clause to the query.
     *
     * @param property The property name.
     * @param values The values to which the property is expected to be equal.
     * @return The Query instance.
     */
    public Query all(String property, Object... values)
    {
        return this.all(property, Arrays.asList(values));
    }

    /**
     * Adds an "all" clause to the query.
     *
     * @param property The property name.
     * @param values The values to which the property is expected to be equal.
     * @return The Query instance.
     */
    public Query all(String property, Collection<?> values)
    {
        if (this.all == null)
            this.all = new HashMap<>();
        this.all.put(property, values);
        return this;
    }

    /**
     * Adds a "not in" clause to the query.
     *
     * @param property The property name.
     * @param values The values to which the property is expected not to be equal.
     * @return The Query instance.
     */
    public Query notIn(String property, Object... values)
    {
        return this.notIn(property, Arrays.asList(values));
    }

    /**
     * Adds a "not in" clause to the query.
     *
     * @param property The property name.
     * @param values The values to which the property is expected not to be equal.
     * @return The Query instance.
     */
    public Query notIn(String property, Collection<?> values)
    {
        if (this.notIn == null)
            this.notIn = new HashMap<>();
        this.notIn.put(property, values);
        return this;
    }

    /**
     * Adds a "greater than" clause to the query.
     *
     * @param property The property name.
     * @param value The value the property is expected to be greater than.
     * @return The Query instance.
     */
    public Query greaterThan(String property, Object value)
    {
        if (this.greaterThan == null)
            this.greaterThan = new HashMap<>();
        this.greaterThan.put(property, value);
        return this;
    }

    /**
     * Adds a "lower than" clause to the query.
     *
     * @param property The property name.
     * @param value The value the property is expected to be lower than.
     * @return The Query instance.
     */
    public Query lowerThan(String property, Object value)
    {
        if (this.lowerThan == null)
            this.lowerThan = new HashMap<>();
        this.lowerThan.put(property, value);
        return this;
    }

    /**
     * Adds a "greater than or equal" clause to the query.
     *
     * @param property The property name.
     * @param value The value the property is expected to be greater than or equal.
     * @return The Query instance.
     */
    public Query greaterThanOrEqual(String property, Object value)
    {
        if (this.greaterThanOrEqual == null)
            this.greaterThanOrEqual = new HashMap<>();
        this.greaterThanOrEqual.put(property, value);
        return this;
    }

    /**
     * Adds a "lower than or equal" clause to the query.
     *
     * @param property The property name.
     * @param value The value the property is expected to be lower than or equal.
     * @return The Query instance.
     */
    public Query lowerThanOrEqual(String property, Object value)
    {
        if (this.lowerThanOrEqual == null)
            this.lowerThanOrEqual = new HashMap<>();
        this.lowerThanOrEqual.put(property, value);
        return this;
    }

    /**
     * Adds an "exists" clause to the query.
     *
     * @param property The property name.
     * @param exists True if the value is expected to exist, false otherwise.
     * @return The Query instance.
     */
    public Query exists(String property, Boolean exists)
    {
        if (this.exists == null)
            this.exists = new HashMap<>();
        this.exists.put(property, exists);
        return this;
    }

    public Object getEqual(String property)
    {
        return this.equal.get(property);
    }

    public Collection<?> getIn(String property)
    {
        return this.in == null ? null : this.in.get(property);
    }

    public Collection<?> getNotIn(String property)
    {
        return this.notIn == null ? null : this.notIn.get(property);
    }

    public Object getNotEqual(String property)
    {
        return this.notEqual == null ? null : this.notEqual.get(property);
    }

    public Object getGreaterThan(String property)
    {
        return this.greaterThan == null ? null : this.greaterThan.get(property);
    }

    public Object getLowerThan(String property)
    {
        return this.lowerThan == null ? null : this.lowerThan.get(property);
    }

    public Object getGreaterThanOrEqual(String property)
    {
        return this.greaterThanOrEqual == null ? null : this.greaterThanOrEqual.get(property);
    }

    public Object getLowerThanOrEqual(String property)
    {
        return this.lowerThanOrEqual == null ? null : this.lowerThanOrEqual.get(property);
    }

    public Boolean getExists(String property)
    {
        return this.exists == null ? null : this.exists.get(property);
    }

    public Map<String, Object> getEqual()
    {
        return this.equal;
    }

    public Map<String, Collection<?>> getIn()
    {
        return this.in;
    }

    public Map<String, Collection<?>> getAll()
    {
        return this.all;
    }

    public Map<String, Collection<?>> getNotIn()
    {
        return this.notIn;
    }

    public Map<String, Object> getNotEqual()
    {
        return this.notEqual;
    }

    public Map<String, Object> getGreaterThan()
    {
        return this.greaterThan;
    }

    public Map<String, Object> getLowerThan()
    {
        return this.lowerThan;
    }

    public Map<String, Object> getGreaterThanOrEqual()
    {
        return this.greaterThanOrEqual;
    }

    public Map<String, Object> getLowerThanOrEqual()
    {
        return this.lowerThanOrEqual;
    }

    public Map<String, Boolean> getExists()
    {
        return this.exists;
    }

    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof Query))
            return false;
        Query that = (Query) object;

        return ((this.equal == that.equal) || Objects.equals(this.equal, that.equal))
            && ((this.in == that.in) || Objects.equals(this.in, that.in))
            && ((this.notIn == that.notIn) || Objects.equals(this.notIn, that.notIn))
            && ((this.notEqual == that.notEqual) || Objects.equals(this.notEqual, that.notEqual))
            && ((this.greaterThan == that.greaterThan) || Objects.equals(this.greaterThan, that.greaterThan))
            && ((this.lowerThan == that.lowerThan) || Objects.equals(this.lowerThan, that.lowerThan))
            && ((this.greaterThanOrEqual == that.greaterThanOrEqual) || Objects.equals(this.greaterThanOrEqual, that.greaterThanOrEqual))
            && ((this.lowerThanOrEqual == that.lowerThanOrEqual) || Objects.equals(this.lowerThanOrEqual, that.lowerThanOrEqual))
            && ((this.exists == that.exists) || Objects.equals(this.exists, that.exists))
        ;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(equal, in, notIn, notEqual, greaterThan, lowerThan, greaterThanOrEqual, lowerThanOrEqual, exists);
    }
}
