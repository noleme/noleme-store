package com.noleme.store.query;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * This class is used in query descriptions as a mean to differentiate simple value queries from references to store-specific IDs.
 *
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 07/06/2018
 * @param <T> The identifier's underlying value type.
 */
public class Identifier <T> implements Identifiable <T>
{
    private final T identifier;

    /**
     *
     * @param value The identifier's underlying value.
     */
    public Identifier(T value)
    {
        if (value == null)
            throw new RuntimeException("The value argument of an Identifier cannot be null");

        this.identifier = value;
    }

    /**
     * Produces an identifier (ID) wrapper from a given value.
     * @param value The identifier's underlying value.
     * @param <T> The identifier's underlying value type.
     * @return An identifier wrapper instance.
     */
    public static <T> Identifier<T> from(T value)
    {
        return new Identifier<>(value);
    }

    public static <T, C extends Collection<T>> List<Identifier<T>> from(C values)
    {
        return values.stream().map(Identifier::from).collect(Collectors.toList());
    }

    public T value()
    {
        return this.identifier;
    }

    public <C> boolean hasType(Class<C> c)
    {
        return c.isAssignableFrom(this.identifier.getClass());
    }

    @Override
    public T getId()
    {
        return this.identifier;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Identifier<?> that = (Identifier<?>) o;
        return Objects.equals(identifier, that.identifier);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(identifier);
    }
}
