package com.noleme.store.query;

import java.util.*;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 20/01/2018
 */
public class Filter
{
    private final Set<String> properties;
    private final boolean includeAll;
    private int skip;
    private int max;
    private String sort;
    private Order order;
    private final Map<String, Integer> hint;
    private final Map<String, List<Integer>> slices;

    /**
     *
     */
    public Filter()
    {
        this(true);
    }

    /**
     *
     * @param includeAll Whether the query should retrieve all of the entity's data or if a whitelist should be used.
     */
    public Filter(boolean includeAll)
    {
        this.properties = new HashSet<>();
        this.includeAll = includeAll;
        this.skip = -1;
        this.max = -1;
        this.sort = null;
        this.order = Order.ASC;
        this.hint = new LinkedHashMap<>();
        this.slices = new HashMap<>();
    }

    /**
     *
     * @param key The name of a property to include in the data resulting from the query.
     * @return The Filter instance.
     */
    public Filter property(String key)
    {
        this.properties.add(key);
        return this;
    }

    /**
     *
     * @param keys Names of a properties to include in the data resulting from the query.
     * @return The Filter instance.
     */
    public Filter property(String... keys)
    {
        Collections.addAll(this.properties, keys);
        return this;
    }

    /**
     *
     * @param skip The amount of records to skip in the store dataset.
     * @return The Filter instance.
     */
    public Filter skip(int skip)
    {
        this.skip = skip;
        return this;
    }

    /**
     *
     * @param max The maximum amount of records to return from the store dataset.
     * @return The Filter instance.
     */
    public Filter max(int max)
    {
        this.max = max;
        return this;
    }

    /**
     *
     * @param key The name of a property on which to perform a sort operation.
     * @param order The sort mode to use on the provided property.
     * @return The Filter instance.
     */
    public Filter sort(String key, Order order)
    {
        this.sort = key;
        this.order = order;
        return this;
    }

    public Set<String> getProperties()
    {
        return this.properties;
    }

    public boolean includeAll()
    {
        return this.includeAll;
    }

    public int skip()
    {
        return this.skip;
    }

    public int max()
    {
        return this.max;
    }

    public String sort()
    {
        return this.sort;
    }

    public Order order()
    {
        return this.order;
    }


    public Map<String, Integer> getHint() {
        return hint;
    }

    public Filter addHintIndex(String indexKey, Integer indexValue) {
        this.hint.put(indexKey, indexValue);
        return this;
    }

    public Map<String, List<Integer>> getSlices() {
        return slices;
    }

    /**
     *
     * @param property Array property to slice
     * @param indexes (max) or (skip,max) to slice.
     * @return
     */
    public Filter slice(String property, Integer... indexes) {
        this.slices.put(property, Arrays.asList(indexes));
        return this;
    }

    /**
     *
     */
    public enum Order
    {
        /** Ascending order */
        ASC(1),
        /** Descending order */
        DESC(-1);

        int code;

        Order(int code)
        {
            this.code = code;
        }

        public int getCode()
        {
            return this.code;
        }
    }

    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof Filter))
            return false;
        Filter that = (Filter) object;

        return this.skip == that.skip
            && this.max == that.max
            && this.order == that.order
            && ((this.sort == that.sort) || Objects.equals(this.sort, that.sort))
            && this.includeAll == that.includeAll
            && ((this.properties == that.properties) || Objects.equals(this.properties, that.properties))
        ;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(properties, includeAll, skip, max, sort, order);
    }
}
