package com.noleme.store.query;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 28/10/2019
 */
public interface IdentityMarker <I, O>
{
    O identify(I input);
}
