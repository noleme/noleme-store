package com.noleme.store.query;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/01/2020
 */
public class IdentifierTests
{
    private static final Identifier<String> idString = Identifier.from("someuniqueidentifier");
    private static final Identifier<Integer> idInteger = Identifier.from(123);
    private static final Identifier<Long> idLong = Identifier.from(123456789L);

    @Test
    public void testValue()
    {
        Assertions.assertEquals("someuniqueidentifier", idString.value());
        Assertions.assertEquals(123, idInteger.value());
        Assertions.assertEquals(123456789L, idLong.value());
    }

    @Test
    public void testHasType()
    {
        Assertions.assertEquals(true, idString.hasType(String.class));
        Assertions.assertEquals(false, idString.hasType(Number.class));
        Assertions.assertEquals(false, idString.hasType(Integer.class));
        Assertions.assertEquals(false, idString.hasType(Long.class));

        Assertions.assertEquals(false, idInteger.hasType(String.class));
        Assertions.assertEquals(true, idInteger.hasType(Number.class));
        Assertions.assertEquals(true, idInteger.hasType(Integer.class));
        Assertions.assertEquals(false, idInteger.hasType(Long.class));

        Assertions.assertEquals(false, idLong.hasType(String.class));
        Assertions.assertEquals(true, idLong.hasType(Number.class));
        Assertions.assertEquals(false, idLong.hasType(Integer.class));
        Assertions.assertEquals(true, idLong.hasType(Long.class));
    }
}
