package com.noleme.store.query;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/01/2020
 */
public class QueryTests
{
    @Test
    public void testEquals()
    {
        Query queryA = new Query();
        Query queryB = new Query();

        Query queryC = new Query("something", true).in("other", Arrays.asList("abc", "def"));
        Query queryD = new Query("something", true).in("other", Arrays.asList("abc", "def"));

        Query queryE = new Query("other", true);

        Assertions.assertEquals(true, queryA.equals(queryB));
        Assertions.assertEquals(true, queryC.equals(queryD));
        Assertions.assertEquals(false, queryC.equals(queryE));
    }
}
