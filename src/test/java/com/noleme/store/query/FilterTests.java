package com.noleme.store.query;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/01/2020
 */
public class FilterTests
{
    @Test
    public void testEquals()
    {
        Filter filterA = new Filter();
        Filter filterB = new Filter();

        Filter filterC = new Filter().max(25).skip(0);
        Filter filterD = new Filter().max(25).skip(0);

        Filter filterE = new Filter().max(25).skip(5);

        Assertions.assertEquals(true, filterA.equals(filterB));
        Assertions.assertEquals(true, filterC.equals(filterD));
        Assertions.assertEquals(false, filterC.equals(filterE));
    }
}
