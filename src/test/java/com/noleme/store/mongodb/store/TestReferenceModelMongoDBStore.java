package com.noleme.store.mongodb.store;

import com.mongodb.BasicDBObject;
import com.noleme.mongodb.MongoDBClient;
import com.noleme.store.factory.Factory;
import com.noleme.store.mongodb.DefaultMongoDBStore;
import com.noleme.store.mongodb.store.model.TestReferenceModel;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * @date 18/06/2018
 */
public class TestReferenceModelMongoDBStore extends DefaultMongoDBStore<TestReferenceModel> implements TestReferenceModelStore
{
    /**
     *
     * @param client
     * @param factory
     */
    public TestReferenceModelMongoDBStore(MongoDBClient client, Factory<BasicDBObject, TestReferenceModel> factory)
    {
        super(client, factory);
    }

    @Override
    protected String getCollectionName()
    {
        return "test_reference_model";
    }

    @Override
    public void createIndexes()
    {

    }
}
