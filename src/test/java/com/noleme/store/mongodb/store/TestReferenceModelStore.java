package com.noleme.store.mongodb.store;

import com.noleme.store.Store;
import com.noleme.store.mongodb.store.model.TestReferenceModel;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/01/2020
 */
public interface TestReferenceModelStore extends Store<TestReferenceModel>
{
}
