package com.noleme.store.mongodb.store.factory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.noleme.store.factory.Factory;
import com.noleme.store.mongodb.MongoDBFactories;
import com.noleme.store.mongodb.store.model.TestModel;
import org.bson.types.ObjectId;

import java.util.ArrayList;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/01/2020
 */
public class TestModelFactory implements Factory<BasicDBObject, TestModel>
{
    @Override
    public TestModel build(BasicDBObject input)
    {
        return new TestModel()
            .setId(input.getObjectId("_id").toHexString())
            .setLabel(input.getString("label"))
            .setList(MongoDBFactories.buildCollection(
                new ArrayList<>(),
                i -> ((BasicDBObject)i).getString("value"),
                (BasicDBList) input.get("list"))
            )
        ;
    }

    @Override
    public BasicDBObject transcript(TestModel input)
    {
        BasicDBObject object = new BasicDBObject()
            .append("label", input.getLabel())
            .append("list", MongoDBFactories.transcriptCollection(
                input.getList(),
                v -> new BasicDBObject("value", v))
            )
        ;

        if (input.getId() != null)
            object.put("_id", new ObjectId(input.getId()));

        return object;
    }
}
