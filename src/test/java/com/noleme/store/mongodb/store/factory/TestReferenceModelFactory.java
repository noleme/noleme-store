package com.noleme.store.mongodb.store.factory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.noleme.store.factory.Factory;
import com.noleme.store.mongodb.MongoDBFactories;
import com.noleme.store.mongodb.store.model.TestModel;
import com.noleme.store.mongodb.store.model.TestReferenceModel;
import org.bson.types.ObjectId;

import java.util.ArrayList;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 22/04/2020
 */
public class TestReferenceModelFactory implements Factory<BasicDBObject, TestReferenceModel>
{
    @Override
    public TestReferenceModel build(BasicDBObject input)
    {
        return new TestReferenceModel()
            .setId(input.getObjectId("_id").toHexString())
            .setLabel(input.getString("label"))
            .setList(MongoDBFactories.buildCollection(
                new ArrayList<>(),
                id -> new TestModel().setId(((ObjectId) id).toHexString()),
                (BasicDBList) input.get("list"))
            )
        ;
    }

    @Override
    public BasicDBObject transcript(TestReferenceModel input)
    {
        BasicDBObject object = new BasicDBObject()
            .append("label", input.getLabel())
            .append("list", MongoDBFactories.transcriptCollection(
                input.getList(),
                item -> new ObjectId(item.getId())
            ))
        ;

        if (input.getId() != null)
            object.put("_id", new ObjectId(input.getId()));

        return object;
    }
}
