package com.noleme.store.mongodb.store;

import com.mongodb.BasicDBObject;
import com.noleme.mongodb.MongoDBClient;
import com.noleme.store.factory.Factory;
import com.noleme.store.mongodb.store.model.TestModel;
import com.noleme.store.mongodb.store.model.TestReferenceModel;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * @date 18/06/2018
 */
public class TestReferenceModelEagerMongoDBStore extends TestReferenceModelMongoDBStore
{
    private final TestModelStore testModelStore;

    /**
     *
     * @param client
     * @param factory
     */
    public TestReferenceModelEagerMongoDBStore(MongoDBClient client, Factory<BasicDBObject, TestReferenceModel> factory, TestModelStore testModelStore)
    {
        super(client, factory);
        this.testModelStore = testModelStore;
    }

    @Override
    protected <C extends Collection<TestReferenceModel>> void afterRetrieval(C collection)
    {
        Set<String> ids = new HashSet<>();
        for (TestReferenceModel item : collection)
        {
            ids.addAll(item.getList().stream()
                .map(TestModel::getId)
                .collect(Collectors.toList())
            );
        }

        Map<String, TestModel> models = this.testModelStore.map(ids);

        for (TestReferenceModel item : collection)
        {
            item.setList(item.getList().stream()
                .map(model -> models.get(model.getId()))
                .collect(Collectors.toList())
            );
        }
    }
}
