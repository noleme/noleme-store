package com.noleme.store.mongodb.store;

import com.mongodb.BasicDBObject;
import com.noleme.mongodb.MongoDBClient;
import com.noleme.store.factory.Factory;
import com.noleme.store.mongodb.DefaultMongoDBStore;
import com.noleme.store.mongodb.store.model.TestModel;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * @date 18/06/2018
 */
public class TestModelMongoDBStore extends DefaultMongoDBStore<TestModel> implements TestModelStore
{
    /**
     *
     * @param client
     * @param factory
     */
    public TestModelMongoDBStore(MongoDBClient client, Factory<BasicDBObject, TestModel> factory)
    {
        super(client, factory);
    }

    @Override
    protected String getCollectionName()
    {
        return "test_model";
    }

    @Override
    public void createIndexes()
    {

    }
}
