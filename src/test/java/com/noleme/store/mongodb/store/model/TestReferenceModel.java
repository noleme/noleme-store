package com.noleme.store.mongodb.store.model;

import com.noleme.store.query.Identifiable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 22/04/2020
 */
public class TestReferenceModel implements Identifiable<String>
{
    private String id;
    private String label;
    private List<TestModel> list;

    public TestReferenceModel()
    {
        this.list = new ArrayList<>();
    }

    @Override
    public String getId()
    {
        return this.id;
    }

    public TestReferenceModel setId(String id)
    {
        this.id = id;
        return this;
    }

    public String getLabel()
    {
        return this.label;
    }

    public TestReferenceModel setLabel(String label)
    {
        this.label = label;
        return this;
    }

    public List<TestModel> getList()
    {
        return this.list;
    }

    public TestReferenceModel setList(List<TestModel> list)
    {
        this.list = list;
        return this;
    }

    public TestReferenceModel addListValue(TestModel value)
    {
        this.list.add(value);
        return this;
    }
}
