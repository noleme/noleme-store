package com.noleme.store.mongodb;

import com.noleme.mongodb.MongoDBClient;
import com.noleme.mongodb.MongoDBClientException;
import com.noleme.mongodb.configuration.MongoDBConfiguration;
import com.noleme.mongodb.test.MockDBClient;
import com.noleme.store.mongodb.store.*;
import com.noleme.store.mongodb.store.factory.TestModelFactory;
import com.noleme.store.mongodb.store.factory.TestReferenceModelFactory;
import com.noleme.store.mongodb.store.model.TestModel;
import com.noleme.store.mongodb.store.model.TestReferenceModel;
import com.noleme.store.query.Filter;
import com.noleme.store.query.Query;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/01/2020
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DefaultMongoDBStoreTests
{
    private TestModelStore store;
    private TestReferenceModelStore lazyStore;
    private TestReferenceModelStore eagerStore;

    @BeforeAll
    public void prepareDatabase() throws MongoDBClientException
    {
        MongoDBClient client = new MockDBClient(new MongoDBConfiguration()
            .set(MongoDBConfiguration.DATABASE, "test")
        );

        this.store = new TestModelMongoDBStore(client, new TestModelFactory());
        this.lazyStore = new TestReferenceModelMongoDBStore(client, new TestReferenceModelFactory());
        this.eagerStore = new TestReferenceModelEagerMongoDBStore(client, new TestReferenceModelFactory(), this.store);

        this.store.put(new TestModel().setLabel("first"));
        this.store.put(new TestModel().setLabel("second").addListValue("v1"));
        this.store.put(new TestModel().setLabel("third").addListValue("v1").addListValue("v2"));
    }

    @Test
    public void testCount()
    {
        Assertions.assertEquals(3, this.store.count());
    }

    @Test
    public void testList()
    {
        List<TestModel> models;

        models = this.store.list(new Query());

        Assertions.assertEquals(3, models.size());
        Assertions.assertEquals("first", models.get(0).getLabel());

        models = this.store.list(new Query(), new Filter().skip(1));

        Assertions.assertEquals(2, models.size());
        Assertions.assertEquals("second", models.get(0).getLabel());

        models = this.store.list(new Query(), new Filter().sort("_id", Filter.Order.DESC));

        Assertions.assertEquals(3, models.size());
        Assertions.assertEquals("third", models.get(0).getLabel());

        List<String> labels;

        labels = ((TestModelMongoDBStore)this.store).list(new Query(), new Filter(), input -> input.getString("label"));

        Assertions.assertEquals(3, labels.size());
        Assertions.assertEquals("first", labels.get(0));
    }

    @Test
    public void testFind()
    {
        List<String> ids = ((TestModelMongoDBStore)this.store).list(new Query(), new Filter().max(3), input -> input.getObjectId("_id").toHexString());

        TestModel first = this.store.find(ids.get(0));

        Assertions.assertEquals("first", first.getLabel());

        TestModel second = this.store.find(ids.get(1));

        Assertions.assertEquals("second", second.getLabel());
        Assertions.assertEquals(Collections.singletonList("v1"), second.getList());

        TestModel third = this.store.find(ids.get(2));

        Assertions.assertEquals("third", third.getLabel());
        Assertions.assertEquals(Arrays.asList("v1", "v2"), third.getList());

        TestModel nonExisting = this.store.find(new ObjectId().toHexString());

        Assertions.assertEquals(null, nonExisting);
    }

    @Test
    public void testHas()
    {
        List<String> ids = ((TestModelMongoDBStore)this.store).list(new Query(), new Filter().max(3), input -> input.getObjectId("_id").toHexString());

        boolean hasFirst = this.store.has(ids.get(0));

        Assertions.assertEquals(true, hasFirst);

        boolean hasNonExisting = this.store.has(new ObjectId().toHexString());

        Assertions.assertEquals(false, hasNonExisting);
    }

    @Test
    public void testMap()
    {
        Map<String, TestModel> models;
        List<String> ids = ((TestModelMongoDBStore)this.store).list(new Query(), new Filter().max(3), input -> input.getObjectId("_id").toHexString());

        models = this.store.map(new Query());

        Assertions.assertEquals(3, models.size());
        Assertions.assertEquals("first", models.get(ids.get(0)).getLabel());

        models = this.store.map(new Query(), new Filter().skip(1));

        Assertions.assertEquals(2, models.size());
        Assertions.assertEquals(null, models.get(ids.get(0)));
        Assertions.assertEquals("second", models.get(ids.get(1)).getLabel());

        Map<String, String> labels;

        labels = ((TestModelMongoDBStore)this.store).map(
            new Query(), new Filter(),
            input -> input.getString("label"),
            input -> input.getObjectId("_id").toHexString()
        );

        Assertions.assertEquals(3, labels.size());
        Assertions.assertEquals("first", labels.get(ids.get(0)));
        Assertions.assertEquals("second", labels.get(ids.get(1)));
    }

    @Test
    public void testPutAndRemove()
    {
        this.store.put(new TestModel().setLabel("fourth"));
        this.store.put(new TestModel().setLabel("fifth").addListValue("v1"));

        List<TestModel> models;

        models = this.store.list(new Query());

        Assertions.assertEquals(5, models.size());
        Assertions.assertEquals("fourth", models.get(3).getLabel());
        Assertions.assertEquals("fifth", models.get(4).getLabel());
        Assertions.assertEquals(Collections.singletonList("v1"), models.get(4).getList());

        this.store.remove(models.get(3).getId());
        this.store.remove(models.get(4).getId());

        models = this.store.list(new Query());

        Assertions.assertEquals(3, models.size());
    }

    @Test
    public void testLazyAndEagerLoading()
    {
        List<TestModel> references = this.store.list(new Query());

        TestReferenceModel model = new TestReferenceModel()
            .setLabel("label")
            .setList(references)
        ;

        this.lazyStore.put(model);

        model = this.lazyStore.find(new Query());

        Assertions.assertNotNull(model);
        Assertions.assertEquals(3, model.getList().size());
        Assertions.assertNotNull(model.getList().get(0).getId());
        Assertions.assertNull(model.getList().get(0).getLabel());

        model = this.eagerStore.find(new Query());

        Assertions.assertNotNull(model);
        Assertions.assertEquals(3, model.getList().size());
        Assertions.assertNotNull(model.getList().get(0).getId());
        Assertions.assertNotNull(model.getList().get(0).getLabel());
    }
}
