# Noleme Store

_Last updated for v1.7.1_

This library is host to a collection of opinionated contracts meant to ease the process of creating agnostic `Store` classes and their MongoDB-backed implementations.

Implementations found in this package shouldn't be tied to any specific Noleme project.

_Note: This library is considered as "in beta" and as such significant API changes may occur without prior warning._

## I. Installation

Add the following in your `pom.xml`:

```xml
<dependency>
    <groupId>com.noleme</groupId>
    <artifactId>noleme-store</artifactId>
    <version>1.7.1</version>
</dependency>
```

## II. Notes on Structure and Design

This library comes with a variety of features such as:

* a `Store`, implementation agnostic base interface providing a variety of typical CRUD methods
* the `Query` and `Filter` classes, which are implementation-agnostic representations of query, filtering, sort and data projection requirements.
* the `Builder`, `Transcriber` and `Factory` interfaces for designing classes responsible for exchanging data between POJOs and the store implementation model (eg. MongoDB `BasicDBObject`). 
* the `DefaultMongoDBStore` and `MongoDBFactories` classes providing helper methods and implementations that should fit the vast majority of common cases.

_TODO_

## III. Usage

_TODO_

## IV. Dev Installation

### A. Pre-requisites

This project will require you to have the following:

* Git (versioning)
* Maven (dependency resolving, publishing and packaging) 

### B. Setup

_TODO_
